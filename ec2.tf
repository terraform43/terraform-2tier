# INSTANCES BLOCK - EC2 and DATABASE

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}


resource "aws_instance" "ec2" {
   count                   = 2
   ami                     = data.aws_ami.amazon_linux.id #var.ec2_instance_ami
   instance_type           = var.ec2_instance_type
   availability_zone       = var.az[count.index]
   subnet_id               = aws_subnet.public_subnet[count.index].id
   vpc_security_group_ids  = [aws_security_group.webserver_sg.id] 
   key_name = var.priv_key
   user_data               = <<EOF
       #!/bin/bash
       yum update -y
       yum install -y httpd
       amazon-linux-extras enable php8.1
       yum clean metadata
       yum install php-cli php-pdo php-fpm php-json php-mysqlnd -y
       wget https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1-en.php -O /var/www/html/adm.php
       EC2AZ=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
       echo '<center><h1>This Amazon EC2 instance is located in Availability Zone:AZID </h1></center>' > /var/www/html/index.txt
       sed "s/AZID/$EC2AZ/" /var/www/html/index.txt > /var/www/html/index.html
       chown -R apache:apache /var/www/html
       systemctl start httpd
       systemctl enable httpd
       EOF 

   tags = {
      Name = "ec2_${count.index}"
  }
}
