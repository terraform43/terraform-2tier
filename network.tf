# VPC BLOCK

# creating VPC
resource "aws_vpc" "custom_vpc" {
   cidr_block       = var.vpc_cidr

   tags = {
      Name = "custom_vpc"
   }
}


# public subnets - web instances
resource "aws_subnet" "public_subnet" {
   count             = 2
   vpc_id            = aws_vpc.custom_vpc.id
   cidr_block        = var.cidr_public[count.index]
   availability_zone = var.az[count.index]

   tags = {
      Name = "public_subnet${count.index}"
   }
}

# private subnets - db
resource "aws_subnet" "private_subnet" {
   count             = 2
   vpc_id            = aws_vpc.custom_vpc.id
   cidr_block        = var.cidr_private[count.index]
   availability_zone = var.az[count.index]

   tags = {
      Name = "private_subnet${count.index}"
   }
}

# RDS subnet group
resource "aws_db_subnet_group" "rds_subnet_g" {
  name       = "rds_subnet_g"
  subnet_ids = aws_subnet.private_subnet[*].id

  tags = { 
     Name = "rds_subnet_g"
  }
}


# ROUTING
########################################################################
# internet gateway 
resource "aws_internet_gateway" "igw" {
   vpc_id = aws_vpc.custom_vpc.id

   tags = {
      Name = "igw"
   }
}


# route table
resource "aws_route_table" "rt" {
   vpc_id = aws_vpc.custom_vpc.id
   route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
      Name = "rt"
  }
}


# tags are not allowed here 
# associate route table to the public subnet
resource "aws_route_table_association" "public_rt1" {
   count = 2
   subnet_id      = aws_subnet.public_subnet[count.index].id
   route_table_id = aws_route_table.rt.id
}


# tags are not allowed here 
# associate route table to the private subnet
resource "aws_route_table_association" "private_rt1" {
   count = 2
   subnet_id      = aws_subnet.private_subnet[count.index].id
   route_table_id = aws_route_table.rt.id
}




