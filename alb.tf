# ALB BLOCK
#######################################################################
# only alpha numeric and hyphen is allowed in name
# alb target group
resource "aws_lb_target_group" "external_target_g" {
  name        = "external-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.custom_vpc.id
  # for lb - app - db connection in php script to function
  stickiness {
    type = "lb_cookie"
    enabled = true
  }
}


resource "aws_lb_target_group_attachment" "ec2_target_g" {
  count             = 2
  target_group_arn  = aws_lb_target_group.external_target_g.arn
  target_id         = aws_instance.ec2[count.index].id
  port              = 80
}


# ALB
resource "aws_lb" "external_alb" {
  name                = "external-ALB"
  internal            = false
  load_balancer_type  = "application"
  security_groups     = [aws_security_group.lb_sg.id]
  subnets             = [for subnet in aws_subnet.public_subnet : subnet.id] 
  tags = {
      Name = "external-ALB"
  }
}


# create ALB listener
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.external_alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.external_target_g.arn
  }
}


resource "aws_lb_listener_rule" "adm_rule" {
  count             = length(var.allowed_ip_addresses)
  listener_arn      = aws_lb_listener.alb_listener.arn
  priority          = count.index + 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.external_target_g.arn
  }

  condition {
    path_pattern {
      values = ["/adm.php"]
    }
  }
  condition	{
    source_ip {
      values = [var.allowed_ip_addresses[count.index]]
    }
  }
}

resource "aws_lb_listener_rule" "default_rule" {
  listener_arn      = aws_lb_listener.alb_listener.arn
  priority          = length(var.allowed_ip_addresses) + 2 


    condition {
        path_pattern {
            values = ["/adm.php"]
        }
    }

    action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Access Denied"
      status_code  = "403"
    }
  }	
}

