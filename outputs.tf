# OUTPUTS

output "alb_dns_name" {
  description = "DNS name of the load balancer"
  value       = "${aws_lb.external_alb.dns_name}"
}

output "db_name" {
  description = "DNS name of the database"
  value       = "${aws_db_instance.my_db.endpoint}"
}

output "db_connect_string" {
  description = "MyRDS database connection string"
  value       = "server=${aws_db_instance.my_db.address}; database=ExampleDB; Uid=${var.db_username}; Pwd=${var.db_password}"
  sensitive   = true
}

output "ec2_0_ip" {
  description = "IP address of instance 1"
  value       = "${aws_instance.ec2[0].public_ip}"
}

output "ec2_1_ip" {
  description = "IP address of instance 2"
  value       = "${aws_instance.ec2[1].public_ip}"
}