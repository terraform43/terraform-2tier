# Terraform 2tier

## Description: two-tier architecture with terraform
************************************************************
 - custom VPC
 - 2 public subnets in different AZs for high availability
 - 2 private subnets in different AZs
 - RDS MySQL instance (micro)
 - 1 Load balancer
 - 1 EC2 t2.micro instance in each public subnet
*************************************************************

# Deploy:
`terraform apply -var-file="secrets.tfvars"`